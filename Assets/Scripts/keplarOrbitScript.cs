﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keplarOrbitScript : MonoBehaviour {

    public const float AstronomicalUnit = 149597871.0f;

    public GameObject centralBody;

    public GameObject orbitingBody;

    public float semiMajorAxis = 40.0f;

    public float angle = 5.0f;

    public float radialDistance;

    public float eccentricity;

    float newX;

    float newY;

	// Use this for initialization
	void Start ()
    {
    
	}
    public float scalar = 1.0f;
	// Update is called once per frame
	void Update ()
    {
        angle += 0.01f;

        radialDistance = semiMajorAxis * AstronomicalUnit * (1 - Mathf.Pow(eccentricity, 2)) / (1 + eccentricity * Mathf.Cos(angle)) / scalar;

        newX = radialDistance * Mathf.Cos(angle);

        newY = radialDistance * Mathf.Sin(angle);

        orbitingBody.transform.position = new Vector3(newX, 0, newY);

        // Ensures max float is never exceeded by clamping between 0-2PI
        //angle = angle % (Mathf.PI * 2);
    }
}
